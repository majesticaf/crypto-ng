import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  result: any;

  constructor(private _http: Http) { }

  getPrices() {
    return this._http.get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,LTC,NMC,STC,BCN,PPC,DOGE,FTC,GRC,XPM,XRP,NXT,AUR,DASH,NEO,MZC,XMR,XEM,POT,TIT,XVG,XLM,VTC,ETH,ETC,USDT,DCR,WAVES,ZEC,BCC,BCH,EOS,ADA,BTCP&tsyms=ZAR')
    .map(result => this.result = result.json());
  }

}
