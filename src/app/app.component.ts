import { Component } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Live Cryptocurrency Converter';
  objectKeys = Object.keys;
  cryptos: any;
  app: string = 'Angular';
  name: string = 'Derick van den Berg';

  constructor(private _data: DataService) {

  }
  ngOnInit() {
    this._data.getPrices()
    .subscribe(res => {
      this.cryptos = res;
    });
  }
}
